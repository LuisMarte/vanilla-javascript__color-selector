const products = [
    {
        color:"#000",
        path:"img/",
        image:"black.png"
    },
    {
        color:"#28a355",
        path:"img/",
        image:"green.png"
    },
    {
        color:"#dc0b32",
        path:"img/",
        image:"red.png"
    },
    {
        color:"#3c52b7",
        path:"img/",
        image:"blue.png"
    }
];

//const counter = 0;

function createButtons(){
    products.map((e, i)=>{
    let button = document.createElement('button');
        button.id = i;
        button.style.background = products[i].color;
        button.addEventListener('click', this.changeColor.bind(this), false);
        document.querySelector('.color-selectors').appendChild(button);
    })
}


function changeColor(e) {
   let img = document.querySelector('#product-img');
   img.src = products[e.target.id].path + products[e.target.id].image;
   const buttons=document.querySelector(".color-selectors").children;
    
    for (let index in products) {
        if (buttons[index].id == e.target.id) {
            buttons[index].classList.add("active");
        } else{
            buttons[index].classList.remove("active");
        }
    }

}
document.onload = createButtons();